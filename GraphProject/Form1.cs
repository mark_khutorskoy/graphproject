﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GraphProject
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            graph = new Berzh(new List<Vertex>());
            cbTypeFile.SelectedIndex = 0;
            graphUndirected = new Berzh(new List<Vertex>());
        }


        Berzh graph;
        Import Berzh = new Import();
        List<double> distances; //список расстояний (для диаметра, радиуса)

        Berzh graphUndirected;
        bool[] used;
        //List<string> used = new List<string>();


        private void btnOk_Click(object sender, EventArgs e)
        {
            string input_type = cbTypeFile.SelectedItem.ToString();
            
            switch (input_type)
            {
                case "Берж / список смежности":
                        graph = Berzh.Read_Berzh("Berzh.txt"); //записали данные в граф
                        AllInfo();  
                    break;

                case "Матрица смежности" :
                        graph = Berzh.Read_SmezhMatrix("SmezhMatrix.txt"); //записали данные в граф
                        AllInfo();
                    break;

                case "Матрица инцидентности":
                        graph = Berzh.Read_InziMatrix("InziMatrix.txt"); //записали данные в граф
                        AllInfo();
                    break;

                case "Список дуг":
                        graph = Berzh.Read_Arcs("Arcs.txt"); //записали данные в граф
                        AllInfo();
                    break;
            }
        }

        public void AllInfo ()
        {
            Visualizator.Draw(pbGraph, graph);
            tbVertexNumber.Text = Convert.ToString(graph.List.Count()); //кол-во вершин

            int counter = 0;
            int max_count = 0;
            for (int i = 0; i < graph.List.Count; i++) //кол-во ребер
            {

                counter += graph.List[i].AdjacentList.Count;
                if (graph.List[i].AdjacentList.Count > max_count || max_count==0) max_count = graph.List[i].AdjacentList.Count;

            }
            tbEdgeCount.Text = Convert.ToString(counter); // ребра
            tbMaxDegree.Text = Convert.ToString(max_count); // макс. степень графа

            AllDistances();
            tbDiameter.Text = Convert.ToString(distances.Max()); //диаметр
            tbRadius.Text = Convert.ToString(distances.Min()); //радиус

            tbSvyaz.Text = Convert.ToString(connected_components_amount_dfs());
            tbCycleNumber.Text = Convert.ToString(Convert.ToInt32(tbSvyaz.Text) + EdgesCount() - graph.List.Count);


        }

        public int EdgesCount()
        {
            int counter = 0;

            for(int i=0; i<graph.List.Count; i++)
                for (int j = 0; j < graph.List[i].AdjacentList.Count; j++)
                   // if(graph.List[i].Label!=graph.List[i].AdjacentList[j].Label) //не считаем петли
                    counter++;
        
            return counter;
        }

        public double[] FordBellman(string start, Berzh graph)
        {
            double[] d = new double[graph.List.Count];
            for (int i = 0; i < d.Length; i++)
                d[i] = Double.PositiveInfinity;
            d[graph.List.FindIndex(x => x.Label == start)] = 0;
            for (int k = 1; k < d.Length; k++)
            {
                //перебор дуг
                for (int i = 0; i < graph.List.Count; i++)
                    for (int j = 0; j < graph.List[i].AdjacentList.Count; j++)
                    {
                        var e = graph.List[i].AdjacentList[j];
                        var index = graph.List.FindIndex(x => x.Label == e.Label);
                        if (d[index] > d[i] + e.Weight) d[index] = d[i] + e.Weight;
                    }
            }
            return d;
        }

        public double[] FordBellman(string start)
        {
            return FordBellman(start, this.graph);
        }

        public void AllDistances()
        {
            distances = new List<double>();
            for (int i=0; i<graph.List.Count; i++)
            {
               distances.Add(FordBellman(graph.List[i].Label).Max()); //нашли максимумы от всех вершин.
            }
        }

        public void ConvertToUndirected()
        {
            graphUndirected.List.Clear();
            for (int i = 0; i<graph.List.Count; i++)
            {
                graphUndirected.List.Add(new Vertex(graph.List[i].Label, new List<Edge>())); //скопировали вертексы
            }

            for (int i = 0; i < graph.List.Count; i++)
            {
                for (int j = 0; j < graph.List[i].AdjacentList.Count; j++)
                {
                    graphUndirected.List[i].AdjacentList.Add(new Edge(graph.List[i].AdjacentList[j].Label, graph.List[i].AdjacentList[j].Weight));
                }
            }

            for (int i = 0; i < graph.List.Count; i++)
            {
                for (int j = 0; j < graph.List[i].AdjacentList.Count; j++)
                {
                    var cur_vertex = graphUndirected.List.FindIndex(x => x.Label == graph.List[i].AdjacentList[j].Label);
                    graphUndirected.List[cur_vertex].AdjacentList.Add(new Edge(graph.List[i].Label, graph.List[i].AdjacentList[j].Weight));
                }
            }

        }

        public void DFS(int cur_vertex)
        {
            used[cur_vertex] = true; //пометили использованной
            for (int i=0; i<graphUndirected.List[cur_vertex].AdjacentList.Count; i++)
            {
                string next = graphUndirected.List[cur_vertex].AdjacentList[i].Label;
                var j = graphUndirected.List.FindIndex(x => x.Label == next);
                if (!used[j]) DFS(j);
            }
        }
        public void DFS(int cur, ref List<string> labels)
        {
            used[cur] = true; labels.Add(graph.List[cur].Label);
            for (int i = 0; i < graph.List[cur].AdjacentList.Count; i++)
            {
                string next = graph.List[cur].AdjacentList[i].Label;
                int nxt = graph.List.FindIndex(x => x.Label == next);
                if (!used[nxt]) DFS(nxt, ref labels);
            }
        
        }

        public int connected_components_amount_dfs()
        {
            ConvertToUndirected();
            used = new bool[graphUndirected.List.Count];
            int cnt = 0;
            for (int i = 0; i < graphUndirected.List.Count; i++)
            {
                if (!used[i])
                {
                    DFS(i);
                    cnt++;
                }
            }
            return cnt;
        }

        public string PrintDFS(string start)
        {
            string s = "";
            List<string> li = new List<string>();
            used = new bool[graph.List.Count];
            if (start == "") return "Обход невозможен";
            int index = graph.List.FindIndex(x => x.Label == start);
            if (index == -1) return "Обход невозможен";
            DFS(index, ref li);
            //for (int i = 0; i < list.List.Count; i++)
            //{
            //    if (!used[i]) DFS(i, used, list, ref li);
            //}
            for (int i = 0; i < li.Count - 1; i++)
            {
                s += li[i] + " -> ";
            }
            s += li[li.Count - 1];
            return s;
        }

        private void pbAddVertex_Click(object sender, EventArgs e)
        {
            graph.List.Add(new Vertex(tbAddVertex.Text, new List<Edge>()));
            AllInfo();
        }

        private void pbDeleteVertex_Click(object sender, EventArgs e)
        {
           
            Vertex vertmp = null;
            for (int i=0; i<graph.List.Count;i++)
            {
                for(int j=graph.List[i].AdjacentList.Count-1; j>=0; j--)
                    if (graph.List[i].AdjacentList[j].Label == tbDeleteVertex.Text) graph.List[i].AdjacentList.RemoveAt(j);

                if (graph.List[i].Label == tbDeleteVertex.Text) vertmp = graph.List[i];
            }
            if (vertmp!=null) graph.List.Remove(vertmp);
            AllInfo();
        }

        private void pbAddEdge_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < graph.List.Count; i++)
            {
                if (graph.List[i].Label == tbAddV1.Text)
                {
                    graph.List[i].AdjacentList.Add(new Edge(tbAddV2.Text, Convert.ToDouble(tbAddWeight.Text)));
                    break;
                }
            }
            AllInfo();

        }

        private void pbDeleteEdge_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < graph.List.Count; i++)
            {
                if (graph.List[i].Label == tbDeleteV1.Text) //нашли вершину 1
                {
                    for (int j=0; j<graph.List[i].AdjacentList.Count; j++)
                    {
                        if (graph.List[i].AdjacentList[j].Label == tbDeleteV2.Text) //нашли в списке смежности
                        {
                            graph.List[i].AdjacentList.RemoveAt(j);
                            break;
                        }
                    } 
                }
            }
            AllInfo();
        }

        private void btnGetIsEdge_Click(object sender, EventArgs e)
        {
            string result = "Ребра нет";
            for (int i = 0; i<graph.List.Count; i++)
            {
                if (graph.List[i].Label == tbIsEdgeV1.Text)//значит есть вершина
                {
                    for (int j = 0; j < graph.List[i].AdjacentList.Count; j++)
                        if (graph.List[i].AdjacentList[j].Label == tbIsEdgeV2.Text) //значит ребро есть
                        {
                            result = "Ребро есть, его вес " + Convert.ToString(graph.List[i].AdjacentList[j].Weight);
                            break;
                        }
                }
            }
            MessageBox.Show(result);
        }

        private void btnGetAdjacentList_Click(object sender, EventArgs e)
        {
            string result = "";
            for (int i = 0; i < graph.List.Count; i++)
            {
                if (graph.List[i].Label == tbAdjacentList.Text)//значит есть вершина
                {
                    for (int j = 0; j < graph.List[i].AdjacentList.Count; j++)
                    {
                        result = result + " " + graph.List[i].AdjacentList[j].Label;
                    }    
                    MessageBox.Show (result);
                    break;
                }
            }
            if (result=="") MessageBox.Show("Нет смежных вершин");
        }

        private void IsATree_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(tbCycleNumber.Text) == 0) MessageBox.Show("Дерево!");
            else MessageBox.Show("Не дерево");
        }

        public double[,] GetDistanceMatrix(string OrieType)
        {
            if (OrieType == "ориентированный")
            {
                double[,] DistanceMatrix = new double[graph.List.Count, graph.List.Count]; //матрица расстояний
                double[] curVert = new double[graph.List.Count];
                for (int i = 0; i < graph.List.Count; i++)
                {
                    curVert = FordBellman(graph.List[i].Label);
                    for (int j = 0; j < graph.List.Count; j++)
                        DistanceMatrix[i, j] = curVert[j];
                }
                return DistanceMatrix;
            }
            else
            {
                double[,] DistanceMatrix = new double[graphUndirected.List.Count, graphUndirected.List.Count]; //матрица расстояний
                double[] curVert = new double[graphUndirected.List.Count];
                for (int i = 0; i < graphUndirected.List.Count; i++)
                {
                    curVert = FordBellman(graphUndirected.List[i].Label, graphUndirected);
                    for (int j = 0; j < graphUndirected.List.Count; j++)
                        DistanceMatrix[i, j] = curVert[j];
                }
                //вывод матрицы расстояний
                //string s = "";
                //for (int i=0; i<graphUndirected.List.Count; i++)
                //{
                //    for (int j=0; j<graphUndirected.List.Count; j++)
                //    {
                //        s += DistanceMatrix[i, j].ToString() + " ";
                //    }
                //    s += Environment.NewLine;
                //}
                //MessageBox.Show(s);
                return DistanceMatrix;
            }
        }

        private void btnAbsCenter_Click(object sender, EventArgs e)
        {

            ConvertToUndirected();// сделали неориентированный
            Visualizator.Draw(pbGraph, graphUndirected);
            if (connected_components_amount_dfs() != 1) MessageBox.Show("Граф должен быть связным!", "Ошибка");
            else
            {
                var outputText = new List<string>();
                var D = GetDistanceMatrix("неориентированный"); ////вызывали матрицу расстояний
                var vertexAndRad = new Dictionary<string, double>();
                double minmax = int.MaxValue; // расстояние от абсолютного центра r до самой удаленной вершины
                int r = 0;
                double MAX = 0;
                int countVert = graphUndirected.List.Count;
                for (int i = 0; i < countVert; i++)
                {
                    MAX = D[i, 0];
                    for (int j = 1; j < countVert; j++)
                    {
                        if (D[i, j] > MAX) MAX = D[i, j];
                    }
                    vertexAndRad.Add(graphUndirected.List[i].Label, MAX);
                    if (minmax > MAX)
                    {
                        r = i;
                        minmax = MAX;
                    }
                }
                double centerRad = minmax;
                var vertices = new List<string>();
                foreach (var elem in vertexAndRad.Keys)
                    if (vertexAndRad[elem] == minmax) vertices.Add(elem);
                double di = 0, dj = 0;
                for (int i = 0; i < countVert - 1; i++)
                    for (int j = i + 1; j < countVert; j++)
                    {
                        Vertex Ej = graphUndirected.List[j];
                        if (graphUndirected.List[i].AdjacentList.FindIndex(x => x.Label == Ej.Label)!=-1) //дуга существует
                        {
                            double MaxI = 0; //расстояние от i до самой удаленной вершины
                            double MaxJ = 0; //расстояние от j до самой удаленной вершины
                            for (int k = 0; k < countVert; k++)
                                if (k != i && k != j)
                                {
                                    if (D[i, k] < 10000) di = D[i, k];
                                    if (D[j, k] < 10000) dj = D[j, k];
                                    if (di > dj)
                                    {
                                        if (dj > MaxJ) MaxJ = dj;
                                    }
                                    else
                                    {
                                        if (di > MaxI) MaxI = di;
                                    }
                                }
                            var weight = graphUndirected.List[i].AdjacentList.Find(y => y.Label == Ej.Label).Weight;
                            double dist = (double)(MaxI + weight + MaxJ) / 2; //Расстояние между внутренними точками дуги i-j и максимально удаленной от них вершиной равно
                            var x = dist - MaxI;
                            if (dist <= minmax && x > 0 && weight - x > 0) //мягкое условие. Если мягкое условие, то может найти абс. центр с радиусом = радиусу центра в вершине.
                            {
                                if (dist < minmax) outputText.Clear();
                                minmax = dist;
                                outputText.Add("Ребро " + graphUndirected.List[i].Label + "-" + graphUndirected.List[j].Label + ";\n" +
                                    " удаление точки от вершины " + graphUndirected.List[i].Label + ": " + x.ToString() + ";\n" +
                                    " радиус " + minmax.ToString());

                            }
                        }
                    }
                if (outputText.Count() == 0)
                {
                    string s = "";
                    foreach (var elem in vertices) s += elem + " ";
                    MessageBox.Show($"Абсолютный центр находится в вершинах {s}, \n радиус: {minmax}", "Абсолютный центр графа");
                }
                else
                {
                    string s = "";
                    foreach (var elem in outputText) s += elem + "\n";

                    if (minmax == centerRad)
                    {
                        s += "Абсолютный центр графа находится в вершинах:\n";
                        foreach (var elem in vertices) s += elem + " \n";
                        s += $"Радиус = {minmax}\n";
                    }
                    MessageBox.Show($"Абсолютный центр графа: \n{s}", "Абсолютный центр графа");
                }
            }
            Visualizator.Draw(pbGraph, graph);


        }

        private void btnGetBypass_Click(object sender, EventArgs e)
        {
            rtbBypass.Text = PrintDFS(tbBypassVertex.Text);
        }

        private void btnParaSoch_Click(object sender, EventArgs e)
        {
                // var undirected = GraphOperations.ToNonOriented(list);
                ConvertToUndirected();// сделали неориентированный
                var matching = EdmondsMatching.maximumMatching(graphUndirected);
                //DrawGraph.DrawNonOriented(pbGraph, list);
                Visualizator.Draw(pbGraph, graphUndirected);

                List<Edge<string>> answ = new List<Edge<string>>();
                foreach (var i in matching.List)
                {
                    foreach (var j in i.AdjacentList)
                    {
                        if (answ.FindIndex(x => x.start == j.Label && x.end == i.Label) == -1)
                            answ.Add(new Edge<string>(i.Label, j.Label));
                    }
                }
                string s = "Ребра в паросочетании: \n";
                foreach (var i in answ)
                {
                    s += "(" + i.start + ", " + i.end + ")\n";
                }
            // rtb.Text = s;
            // MessageBox.Show("OK");
            MessageBox.Show(s);
            Visualizator.Draw(pbGraph, graph);
            //DrawGraph.Draw(pbGraph, list);

        }
    }
}
