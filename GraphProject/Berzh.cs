﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphProject
{
        public class Vertex
        {
            string label; //имя вершина(метка)
            List<Edge> adjacentlist; //список вершин смежных с данной

            public string Label
            {
                get;
                set;
            }

            public List<Edge> AdjacentList
            {
                get { return adjacentlist; }
                set { adjacentlist = value; }
            }

            public Vertex(string label, List<Edge> adjacentlist)
            {
                this.Label = label;
                this.AdjacentList = adjacentlist;
            }
        }

        public class Edge
        {
            string label;
            double weight;
            public string Label
            {
                get { return label; }
                set { label = value; }
            }
            public double Weight
            {
                get { return weight; }
                set { weight = value; }
            }
            public Edge(string label, double weight)
            {
                this.Label = label;
                this.Weight = weight;
            }
        }

         public class Berzh
        {
            List<Vertex> list;
            public List<Vertex> List
            {
                get { return list; }
                set { list = value; }
            }
            public Berzh(List<Vertex> list)
            {
                this.list = list;
            }
        }
}




       
        