﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphProject
{
    class NodeInformation<T>
    {
        public T parent;
        public T treeRoot;
        public bool isOuter; // True for outer node, false for inner.

        public NodeInformation(T parent, T treeRoot, bool isOuter)
        {
            this.parent = parent;
            this.treeRoot = treeRoot;
            this.isOuter = isOuter;
        }
    }

    class Edge<T>
    {
        public T start;
        public T end;

        public Edge(T start, T end)
        {
            this.start = start;
            this.end = end;
        }
    }

    class Blossom<T>
    {
        public T root; // The root of the blossom; also the representative
        public List<T> cycle; // The nodes, listed in order around the cycle
        public HashSet<T> nodes; // The nodes, stored in a set for efficient lookup

        public Blossom(T root, List<T> cycle, HashSet<T> nodes)
        {
            this.root = root;
            this.cycle = cycle;
            this.nodes = nodes;
        }
    }

    class EdmondsMatching
    {
        public static Berzh maximumMatching(Berzh g)
        {
            if (g.List.Count == 0) return new Berzh(new List<Vertex>());
            Berzh result = new Berzh(new List<Vertex>());

            //добавляем все вершины из графа (одномерный)
            foreach (var node in g.List)
                result.List.Add(new Vertex(node.Label, new List<Edge>()));

            /* Now, continuously iterate, looking for alternating paths between
             * exposed vertices until none can be found.
             */
            while (true)
            {
               //каждый раз увеличивается путь (добавляем пару)
                List<string> path = findAlternatingPath(g, result); //ищем чередующиеся цепь
                if (path == null) return result;

               //собираем из чередующийся цепи паросочетание
                updateMatching(path, result);
            }
        }

        //собираем из чередующийся цепи паросочетание
        private static void updateMatching(List<string> path, Berzh m)
        {
            //создаем обратную цепь
            for (int i = 0; i < path.Count - 1; ++i)
            {
                if (edgeExist(path[i], path[i + 1], m))
                    removeEdge(path[i], path[i + 1], m);
                else
                    addEdge(path[i], path[i + 1], m);
            }
        }

        private static bool edgeExist(string first, string second, Berzh list)
        {
            return list.List.Find(x => x.Label == first).AdjacentList.FindIndex(y => y.Label == second) != -1;
        }

        private static void removeEdge(string first, string second, Berzh list)
        {
            var vert = list.List.Find(x => x.Label == first).AdjacentList;
            vert.Remove(vert.Find(y => y.Label == second));
            vert = list.List.Find(x => x.Label == second).AdjacentList;
            vert.Remove(vert.Find(y => y.Label == first));
        }

        private static void addEdge(string first, string second, Berzh list)
        {
            var vert = list.List.Find(x => x.Label == first).AdjacentList;
            vert.Add(new Edge(second, 1));
            vert = list.List.Find(x => x.Label == second).AdjacentList;
            vert.Add(new Edge(first, 1));
        }

        private static List<string> findAlternatingPath(Berzh g, Berzh m)
        {
            Dictionary<string, NodeInformation<string>> forest = new Dictionary<string, NodeInformation<string>>();
            Queue<Edge<string>> worklist = new Queue<Edge<string>>();

            foreach (var node in m.List)
            {
                //является ли тек. вершина пустой (нет никаких ребер в матчинг)
                if (!(m.List.Find(x => x.Label == node.Label).AdjacentList.Count == 0))
                    continue;
                //добавляем эту вершину к лесу если безреберная в матчинге
                forest.Add(node.Label, new NodeInformation<string>(null, node.Label, true));
                //добавляем все ребра, которые есть в исх. графе (список только ребер), если у вершины нет ребер в матчинге
                foreach (var endpoint in g.List.Find(x => x.Label == node.Label).AdjacentList)
                    worklist.Enqueue(new Edge<string>(node.Label, endpoint.Label));
            }
            //смотрим все ребра
            while (!(worklist.Count == 0))
            {
                Edge<string> curr = worklist.Dequeue();
                if (edgeExist(curr.start, curr.end, m)) //если мы уже добавили в матчинг это ребро, то скип
                    continue;

                NodeInformation<string> startInfo = forest[curr.start]; //добавили  в лес первую вершину тек. ребра
                NodeInformation<string> endInfo = null;
                if (forest.ContainsKey(curr.end)) //добавили ли в лес вторую вершину тек. ребра
                    endInfo = forest[curr.end];

                //если добавили 
                if (endInfo != null)
                {
                    if (endInfo.isOuter && startInfo.treeRoot == endInfo.treeRoot) //стартовая и конеч. вершина одинаковые, стартовая вешина внешняя => есть цветок
                    {
                        Blossom<string> blossom = findBlossom(forest, curr); //сжали блоссом
                        List<string> path = findAlternatingPath(contractGraph(g, blossom), contractGraph(m, blossom)); //ищем path в новом сжатом графе

                        if (path == null) return path;
                        return expandPath(path, g, blossom); //развернули цветок
                    }
                    else if (endInfo.isOuter && startInfo.treeRoot != endInfo.treeRoot) //возвращаем путь по иерархии
                    {
                        List<string> result = new List<string>();
                        for (string node = curr.start; node != null; node = forest[node].parent)
                            result.Add(node);

                        result.Reverse();
    
                        for (string node = curr.end; node != null; node = forest[node].parent)
                            result.Add(node);

                        return result;
                    }
                }
                //если не добавили, то
                else
                {
                    forest.Add(curr.end, new NodeInformation<string>(curr.start, startInfo.treeRoot, false));

                    var end = m.List.Find(x => x.Label == curr.end);
                    if (end != null)
                    {
                        var endpoint = end.AdjacentList[0];
                        forest.Add(endpoint.Label, new NodeInformation<string>(curr.end, startInfo.treeRoot, true));

                        /* Add all outgoing edges from this endpoint to the work
                         * list.
                         */
                        foreach (var fringeNode in g.List.Find(x => x.Label == endpoint.Label).AdjacentList)
                            worklist.Enqueue(new Edge<string>(endpoint.Label, fringeNode.Label));
                    }
                }
            }
            return null;
        }

        private static Blossom<string> findBlossom(Dictionary<string, NodeInformation<string>> forest, Edge<string> edge)
        {

            List<string> onePath = new List<string>(), twoPath = new List<string>();
            for (string node = edge.start; node != null; node = forest[node].parent) onePath.Insert(0, node);
            for (string node = edge.end; node != null; node = forest[node].parent) twoPath.Insert(0, node);

            int mismatch = 0;
            for (; mismatch < onePath.Count && mismatch < twoPath.Count; ++mismatch)
                if (onePath[mismatch] != twoPath[mismatch])
                    break;

            List<string> cycle = new List<string>();
            for (int i = mismatch - 1; i < onePath.Count; ++i) cycle.Add(onePath[i]);
            for (int i = twoPath.Count - 1; i >= mismatch - 1; --i) cycle.Add(twoPath[i]);

            return new Blossom<string>(onePath[mismatch - 1], cycle, new HashSet<string>(cycle));
        }

        private static Berzh contractGraph(Berzh g, Blossom<string> blossom)
        {
            Berzh result = new Berzh(new List<Vertex>());

            foreach (var node in g.List)
            {
                if (!blossom.nodes.Contains(node.Label))
                {
                    var listl = new List<Edge>();
                    listl.AddRange(node.AdjacentList);
                    result.List.Add(new Vertex(node.Label, listl));
                }
            }

            result.List.Add(new Vertex(blossom.root, new List<Edge>()));

            foreach (var node in g.List)
            {
                if (blossom.nodes.Contains(node.Label)) continue;
                var lst = g.List.Find(x => x == node).AdjacentList;
                var endpoint = "";
                for (int i = 0; i < lst.Count; i++)
                {
                    if (blossom.nodes.Contains(lst[i].Label))
                        endpoint = blossom.root;

                  if(result.List.FindIndex(x=>x.Label == endpoint)!=-1)  addEdge(node.Label, endpoint, result);
                }
            }

            return result;
        }

        private static List<string> expandPath(List<string> path, Berzh g, Blossom<string> blossom)
        {
            int index = path.IndexOf(blossom.root);
            if (index == -1) return path;
            if (index % 2 == 1)  path.Reverse();

            List<string> result = new List<string>();
            for (int i = 0; i < path.Count; ++i)
            {
                if (path[i] != blossom.root)
                {
                    result.Add(path[i]);
                }
                else
                {
                    result.Add(blossom.root);

                    string outNode = findNodeLeavingCycle(g, blossom, path[i + 1]);

                    int outIndex = blossom.cycle.IndexOf(outNode);
                    int start = (outIndex % 2 == 0) ? 1 : blossom.cycle.Count - 2;
                    int step = (outIndex % 2 == 0) ? +1 : -1;

                    for (int k = start; k != outIndex + step; k += step)
                        result.Add(blossom.cycle[k]);
                }
            }
            return result;
        }

        private static string findNodeLeavingCycle(Berzh g, Blossom<string> blossom, string node)
        {
            foreach (var cycleNode in blossom.nodes)
                if (edgeExist(cycleNode, node, g)) return cycleNode;
            return null;
        }

    }
}
