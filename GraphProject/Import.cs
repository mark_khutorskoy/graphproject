﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GraphProject
{
    class Import
    {
        public Berzh Read_Berzh(string FileName)
        {
            List<Vertex> graph = new List<Vertex>();

            using (StreamReader sr = new StreamReader(FileName))
            {
                List<Vertex> vertexes = new List<Vertex>();
                while (!sr.EndOfStream)
                {
                    String line = sr.ReadLine();

                    List<Edge> edges = new List<Edge>();

                    string[] split = line.Split(' ');

                    for (int i = 1; i < split.Length; i++)
                    {
                        string[] splitWeight = split[i].Split('/');
                        edges.Add(new Edge(splitWeight[0], Convert.ToDouble(splitWeight[1])));
                    }
                    //занесли вертекс
                    vertexes.Add(new Vertex(split[0], edges));
                }
                return new Berzh(vertexes);

            }


        }

        public Berzh Read_Arcs(string FileName)
        {
            List<Vertex> graph = new List<Vertex>();

            using (StreamReader sr = new StreamReader(FileName))
            {
                List<Vertex> vertexes = new List<Vertex>();
                List<Edge> edges = new List<Edge>();

                String vertexes_list = sr.ReadLine(); //считали набор вершин

                //заполнили список вершин
                string[] splited_vertexes = vertexes_list.Split(' '); 
                for (int i = 0; i < splited_vertexes.Length; i++) vertexes.Add(new Vertex(splited_vertexes[i], new List<Edge>())); 

                while (!sr.EndOfStream)
                {
                    String line = sr.ReadLine();
                    string[] split_edge = line.Split(' ');
                    var i = vertexes.FindIndex(x => x.Label == split_edge[0]); //нашли в списке вершин нужню
                    vertexes[i].AdjacentList.Add(new Edge(split_edge[1], Convert.ToDouble(split_edge[2])));
                }

                return new Berzh(vertexes);
            }
        }

        public Berzh Read_SmezhMatrix(string FileName)
        {
            List<Vertex> graph = new List<Vertex>();
            using (StreamReader sr = new StreamReader(FileName))
            {
                List<Vertex> vertexes = new List<Vertex>();
                List<Edge> edges = new List<Edge>();

                String vertexes_list = sr.ReadLine(); //считали набор вершин

                //заполнили список вершин пропуская 0
                string[] splited_vertexes = vertexes_list.Split(' ');
                for (int i = 1; i < splited_vertexes.Length; i++) vertexes.Add(new Vertex(splited_vertexes[i], new List<Edge>()));

                while (!sr.EndOfStream)
                {
                    String line = sr.ReadLine();
                    string[] split_edge = line.Split(' ');
                    var i = vertexes.FindIndex(x => x.Label == split_edge[0]); //нашли в списке вершин нужню
                    for (int j = 1; j < split_edge.Length; j++)
                    {
                        if (Convert.ToDouble(split_edge[j])!=0)
                            vertexes[i].AdjacentList.Add(new Edge(Convert.ToString(j), Convert.ToDouble(split_edge[j])));
                    }
                }
                return new Berzh(vertexes);
            }
                
        }

        public Berzh Read_InziMatrix(string FileName)
        {
            List<Vertex> graph = new List<Vertex>();
            using (StreamReader sr = new StreamReader(FileName))
            {
                List<Vertex> vertexes = new List<Vertex>();
                List<Edge> edges = new List<Edge>();

                String vertexes_list = sr.ReadLine(); //считали набор вершин
                //заполнили список 
                string[] splited_vertexes = vertexes_list.Split(' ');
                for (int i = 0; i < splited_vertexes.Length; i++) vertexes.Add(new Vertex(splited_vertexes[i], new List<Edge>()));      

                while (!sr.EndOfStream)
                {
                    String line = sr.ReadLine();
                    string[] split_edge = line.Split(' ');

                    string begin = "";
                    string end = "";
                    double weight=0;

                    for (int j = 0; j < split_edge.Length; j++) //идем по строке
                    {
                        if (Convert.ToDouble(split_edge[j]) > 0) //нашли положительный вес
                        {
                            begin = splited_vertexes[j]; //запомнили начало
                            weight = Convert.ToDouble(split_edge[j]); //положительный вес
                        }
                        if (Convert.ToDouble(split_edge[j]) < 0) //нашли отрицательный вес
                        {
                            end = splited_vertexes[j]; //запомнили начало
                        }

                    }
                    var i = vertexes.FindIndex(x => x.Label == begin); //нашли в списке вершин нужню
                    if (end != "")
                    {
                        vertexes[i].AdjacentList.Add(new Edge(end, weight));
                    }
                    else
                    {
                        vertexes[i].AdjacentList.Add(new Edge(begin, weight));
                    }
                }
                return new Berzh(vertexes);

            }


        }
    }
}
