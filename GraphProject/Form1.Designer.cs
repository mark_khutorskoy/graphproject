﻿namespace GraphProject
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.cbTypeFile = new System.Windows.Forms.ComboBox();
            this.gbInfo = new System.Windows.Forms.GroupBox();
            this.tbSvyaz = new System.Windows.Forms.TextBox();
            this.tbCycleNumber = new System.Windows.Forms.TextBox();
            this.tbRadius = new System.Windows.Forms.TextBox();
            this.tbDiameter = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMaxDegree = new System.Windows.Forms.TextBox();
            this.lblMaxDegree = new System.Windows.Forms.Label();
            this.tbEdgeCount = new System.Windows.Forms.TextBox();
            this.lblEdgeCount = new System.Windows.Forms.Label();
            this.tbVertexNumber = new System.Windows.Forms.TextBox();
            this.lblVertexNumber = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pbDeleteEdge = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbDeleteV2 = new System.Windows.Forms.TextBox();
            this.tbDeleteV1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbAddWeight = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbAddV2 = new System.Windows.Forms.TextBox();
            this.tbAddV1 = new System.Windows.Forms.TextBox();
            this.pbAddEdge = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pbDeleteVertex = new System.Windows.Forms.PictureBox();
            this.tbDeleteVertex = new System.Windows.Forms.TextBox();
            this.pbAddVertex = new System.Windows.Forms.PictureBox();
            this.tbAddVertex = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblAddVertex = new System.Windows.Forms.Label();
            this.pbGraph = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnAbsCenter = new System.Windows.Forms.Button();
            this.IsATree = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnGetAdjacentList = new System.Windows.Forms.Button();
            this.tbAdjacentList = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnGetIsEdge = new System.Windows.Forms.Button();
            this.tbIsEdgeV1 = new System.Windows.Forms.TextBox();
            this.tbIsEdgeV2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnGetBypass = new System.Windows.Forms.Button();
            this.rtbBypass = new System.Windows.Forms.RichTextBox();
            this.tbBypassVertex = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnParaSoch = new System.Windows.Forms.Button();
            this.gbInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteEdge)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddEdge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteVertex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddVertex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbGraph)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(19, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Тип файла:";
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOk.Location = new System.Drawing.Point(415, 32);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(131, 30);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Загрузить";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cbTypeFile
            // 
            this.cbTypeFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbTypeFile.FormattingEnabled = true;
            this.cbTypeFile.Items.AddRange(new object[] {
            "Берж / список смежности",
            "Матрица смежности",
            "Матрица инцидентности",
            "Список дуг"});
            this.cbTypeFile.Location = new System.Drawing.Point(138, 32);
            this.cbTypeFile.Name = "cbTypeFile";
            this.cbTypeFile.Size = new System.Drawing.Size(245, 28);
            this.cbTypeFile.TabIndex = 4;
            // 
            // gbInfo
            // 
            this.gbInfo.Controls.Add(this.tbSvyaz);
            this.gbInfo.Controls.Add(this.tbCycleNumber);
            this.gbInfo.Controls.Add(this.tbRadius);
            this.gbInfo.Controls.Add(this.tbDiameter);
            this.gbInfo.Controls.Add(this.label11);
            this.gbInfo.Controls.Add(this.label10);
            this.gbInfo.Controls.Add(this.label8);
            this.gbInfo.Controls.Add(this.label2);
            this.gbInfo.Controls.Add(this.tbMaxDegree);
            this.gbInfo.Controls.Add(this.lblMaxDegree);
            this.gbInfo.Controls.Add(this.tbEdgeCount);
            this.gbInfo.Controls.Add(this.lblEdgeCount);
            this.gbInfo.Controls.Add(this.tbVertexNumber);
            this.gbInfo.Controls.Add(this.lblVertexNumber);
            this.gbInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbInfo.Location = new System.Drawing.Point(12, 81);
            this.gbInfo.Name = "gbInfo";
            this.gbInfo.Size = new System.Drawing.Size(371, 271);
            this.gbInfo.TabIndex = 5;
            this.gbInfo.TabStop = false;
            this.gbInfo.Text = "Информация:";
            // 
            // tbSvyaz
            // 
            this.tbSvyaz.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbSvyaz.Location = new System.Drawing.Point(248, 230);
            this.tbSvyaz.Name = "tbSvyaz";
            this.tbSvyaz.ReadOnly = true;
            this.tbSvyaz.Size = new System.Drawing.Size(100, 26);
            this.tbSvyaz.TabIndex = 14;
            // 
            // tbCycleNumber
            // 
            this.tbCycleNumber.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbCycleNumber.Location = new System.Drawing.Point(248, 197);
            this.tbCycleNumber.Name = "tbCycleNumber";
            this.tbCycleNumber.ReadOnly = true;
            this.tbCycleNumber.Size = new System.Drawing.Size(100, 26);
            this.tbCycleNumber.TabIndex = 13;
            // 
            // tbRadius
            // 
            this.tbRadius.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbRadius.Location = new System.Drawing.Point(248, 165);
            this.tbRadius.Name = "tbRadius";
            this.tbRadius.ReadOnly = true;
            this.tbRadius.Size = new System.Drawing.Size(100, 26);
            this.tbRadius.TabIndex = 12;
            // 
            // tbDiameter
            // 
            this.tbDiameter.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbDiameter.Location = new System.Drawing.Point(248, 131);
            this.tbDiameter.Name = "tbDiameter";
            this.tbDiameter.ReadOnly = true;
            this.tbDiameter.Size = new System.Drawing.Size(100, 26);
            this.tbDiameter.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 230);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(208, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Число комп. связности:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(220, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Цикломатическое число:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Радиус: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Диаметр: ";
            // 
            // tbMaxDegree
            // 
            this.tbMaxDegree.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbMaxDegree.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbMaxDegree.Location = new System.Drawing.Point(248, 99);
            this.tbMaxDegree.Name = "tbMaxDegree";
            this.tbMaxDegree.ReadOnly = true;
            this.tbMaxDegree.Size = new System.Drawing.Size(100, 26);
            this.tbMaxDegree.TabIndex = 6;
            // 
            // lblMaxDegree
            // 
            this.lblMaxDegree.AutoSize = true;
            this.lblMaxDegree.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMaxDegree.Location = new System.Drawing.Point(18, 99);
            this.lblMaxDegree.Name = "lblMaxDegree";
            this.lblMaxDegree.Size = new System.Drawing.Size(203, 20);
            this.lblMaxDegree.TabIndex = 5;
            this.lblMaxDegree.Text = "Макс. степень вершин:";
            // 
            // tbEdgeCount
            // 
            this.tbEdgeCount.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbEdgeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbEdgeCount.Location = new System.Drawing.Point(248, 67);
            this.tbEdgeCount.Name = "tbEdgeCount";
            this.tbEdgeCount.ReadOnly = true;
            this.tbEdgeCount.Size = new System.Drawing.Size(100, 26);
            this.tbEdgeCount.TabIndex = 4;
            // 
            // lblEdgeCount
            // 
            this.lblEdgeCount.AutoSize = true;
            this.lblEdgeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEdgeCount.Location = new System.Drawing.Point(18, 66);
            this.lblEdgeCount.Name = "lblEdgeCount";
            this.lblEdgeCount.Size = new System.Drawing.Size(126, 20);
            this.lblEdgeCount.TabIndex = 3;
            this.lblEdgeCount.Text = "Кол-во ребер:";
            // 
            // tbVertexNumber
            // 
            this.tbVertexNumber.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbVertexNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbVertexNumber.Location = new System.Drawing.Point(248, 35);
            this.tbVertexNumber.Name = "tbVertexNumber";
            this.tbVertexNumber.ReadOnly = true;
            this.tbVertexNumber.Size = new System.Drawing.Size(100, 26);
            this.tbVertexNumber.TabIndex = 2;
            // 
            // lblVertexNumber
            // 
            this.lblVertexNumber.AutoSize = true;
            this.lblVertexNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVertexNumber.Location = new System.Drawing.Point(18, 35);
            this.lblVertexNumber.Name = "lblVertexNumber";
            this.lblVertexNumber.Size = new System.Drawing.Size(140, 20);
            this.lblVertexNumber.TabIndex = 0;
            this.lblVertexNumber.Text = "Кол-во вершин:\r\n";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.pbDeleteVertex);
            this.groupBox1.Controls.Add(this.tbDeleteVertex);
            this.groupBox1.Controls.Add(this.pbAddVertex);
            this.groupBox1.Controls.Add(this.tbAddVertex);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lblAddVertex);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(415, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 441);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Операции";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pbDeleteEdge);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.tbDeleteV2);
            this.groupBox3.Controls.Add(this.tbDeleteV1);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(6, 300);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(366, 126);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Удалить ребро:";
            // 
            // pbDeleteEdge
            // 
            this.pbDeleteEdge.Image = global::GraphProject.Properties.Resources.minus;
            this.pbDeleteEdge.Location = new System.Drawing.Point(320, 57);
            this.pbDeleteEdge.Name = "pbDeleteEdge";
            this.pbDeleteEdge.Size = new System.Drawing.Size(30, 30);
            this.pbDeleteEdge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDeleteEdge.TabIndex = 20;
            this.pbDeleteEdge.TabStop = false;
            this.pbDeleteEdge.Click += new System.EventHandler(this.pbDeleteEdge_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(9, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 21;
            this.label4.Text = "Начало:";
            // 
            // tbDeleteV2
            // 
            this.tbDeleteV2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbDeleteV2.Location = new System.Drawing.Point(189, 68);
            this.tbDeleteV2.Name = "tbDeleteV2";
            this.tbDeleteV2.Size = new System.Drawing.Size(100, 26);
            this.tbDeleteV2.TabIndex = 25;
            // 
            // tbDeleteV1
            // 
            this.tbDeleteV1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbDeleteV1.Location = new System.Drawing.Point(189, 31);
            this.tbDeleteV1.Name = "tbDeleteV1";
            this.tbDeleteV1.Size = new System.Drawing.Size(100, 26);
            this.tbDeleteV1.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(9, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 20);
            this.label9.TabIndex = 22;
            this.label9.Text = "Конец:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbAddWeight);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbAddV2);
            this.groupBox2.Controls.Add(this.tbAddV1);
            this.groupBox2.Controls.Add(this.pbAddEdge);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(6, 132);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(366, 147);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Добавить ребро:";
            // 
            // tbAddWeight
            // 
            this.tbAddWeight.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbAddWeight.Location = new System.Drawing.Point(189, 92);
            this.tbAddWeight.Name = "tbAddWeight";
            this.tbAddWeight.Size = new System.Drawing.Size(100, 26);
            this.tbAddWeight.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(9, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "Начало:";
            // 
            // tbAddV2
            // 
            this.tbAddV2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbAddV2.Location = new System.Drawing.Point(189, 62);
            this.tbAddV2.Name = "tbAddV2";
            this.tbAddV2.Size = new System.Drawing.Size(100, 26);
            this.tbAddV2.TabIndex = 25;
            // 
            // tbAddV1
            // 
            this.tbAddV1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbAddV1.Location = new System.Drawing.Point(189, 31);
            this.tbAddV1.Name = "tbAddV1";
            this.tbAddV1.Size = new System.Drawing.Size(100, 26);
            this.tbAddV1.TabIndex = 24;
            // 
            // pbAddEdge
            // 
            this.pbAddEdge.Image = global::GraphProject.Properties.Resources.plus;
            this.pbAddEdge.Location = new System.Drawing.Point(320, 68);
            this.pbAddEdge.Name = "pbAddEdge";
            this.pbAddEdge.Size = new System.Drawing.Size(30, 30);
            this.pbAddEdge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddEdge.TabIndex = 19;
            this.pbAddEdge.TabStop = false;
            this.pbAddEdge.Click += new System.EventHandler(this.pbAddEdge_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(9, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "Вес:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(9, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 22;
            this.label6.Text = "Конец:";
            // 
            // pbDeleteVertex
            // 
            this.pbDeleteVertex.Image = global::GraphProject.Properties.Resources.minus;
            this.pbDeleteVertex.Location = new System.Drawing.Point(326, 76);
            this.pbDeleteVertex.Name = "pbDeleteVertex";
            this.pbDeleteVertex.Size = new System.Drawing.Size(30, 30);
            this.pbDeleteVertex.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDeleteVertex.TabIndex = 14;
            this.pbDeleteVertex.TabStop = false;
            this.pbDeleteVertex.Click += new System.EventHandler(this.pbDeleteVertex_Click);
            // 
            // tbDeleteVertex
            // 
            this.tbDeleteVertex.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbDeleteVertex.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbDeleteVertex.Location = new System.Drawing.Point(195, 76);
            this.tbDeleteVertex.Name = "tbDeleteVertex";
            this.tbDeleteVertex.Size = new System.Drawing.Size(100, 26);
            this.tbDeleteVertex.TabIndex = 13;
            // 
            // pbAddVertex
            // 
            this.pbAddVertex.Image = global::GraphProject.Properties.Resources.plus;
            this.pbAddVertex.Location = new System.Drawing.Point(326, 35);
            this.pbAddVertex.Name = "pbAddVertex";
            this.pbAddVertex.Size = new System.Drawing.Size(30, 30);
            this.pbAddVertex.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddVertex.TabIndex = 11;
            this.pbAddVertex.TabStop = false;
            this.pbAddVertex.Click += new System.EventHandler(this.pbAddVertex_Click);
            // 
            // tbAddVertex
            // 
            this.tbAddVertex.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbAddVertex.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbAddVertex.Location = new System.Drawing.Point(195, 35);
            this.tbAddVertex.Name = "tbAddVertex";
            this.tbAddVertex.Size = new System.Drawing.Size(100, 26);
            this.tbAddVertex.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(15, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Удалить вершину:";
            // 
            // lblAddVertex
            // 
            this.lblAddVertex.AutoSize = true;
            this.lblAddVertex.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAddVertex.Location = new System.Drawing.Point(15, 35);
            this.lblAddVertex.Name = "lblAddVertex";
            this.lblAddVertex.Size = new System.Drawing.Size(174, 20);
            this.lblAddVertex.TabIndex = 0;
            this.lblAddVertex.Text = "Добавить вершину:";
            // 
            // pbGraph
            // 
            this.pbGraph.Location = new System.Drawing.Point(829, 81);
            this.pbGraph.Name = "pbGraph";
            this.pbGraph.Size = new System.Drawing.Size(363, 540);
            this.pbGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbGraph.TabIndex = 3;
            this.pbGraph.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnParaSoch);
            this.groupBox4.Controls.Add(this.btnAbsCenter);
            this.groupBox4.Controls.Add(this.IsATree);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(13, 358);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(370, 355);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Запросы";
            // 
            // btnAbsCenter
            // 
            this.btnAbsCenter.Location = new System.Drawing.Point(10, 305);
            this.btnAbsCenter.Name = "btnAbsCenter";
            this.btnAbsCenter.Size = new System.Drawing.Size(139, 34);
            this.btnAbsCenter.TabIndex = 8;
            this.btnAbsCenter.Text = "Абс. центр";
            this.btnAbsCenter.UseVisualStyleBackColor = true;
            this.btnAbsCenter.Click += new System.EventHandler(this.btnAbsCenter_Click);
            // 
            // IsATree
            // 
            this.IsATree.Location = new System.Drawing.Point(10, 254);
            this.IsATree.Name = "IsATree";
            this.IsATree.Size = new System.Drawing.Size(139, 34);
            this.IsATree.TabIndex = 8;
            this.IsATree.Text = "Дерево ли?";
            this.IsATree.UseVisualStyleBackColor = true;
            this.IsATree.Click += new System.EventHandler(this.IsATree_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnGetAdjacentList);
            this.groupBox6.Controls.Add(this.tbAdjacentList);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Location = new System.Drawing.Point(6, 167);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(354, 81);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Список смежных вершин";
            // 
            // btnGetAdjacentList
            // 
            this.btnGetAdjacentList.Location = new System.Drawing.Point(241, 39);
            this.btnGetAdjacentList.Name = "btnGetAdjacentList";
            this.btnGetAdjacentList.Size = new System.Drawing.Size(100, 29);
            this.btnGetAdjacentList.TabIndex = 10;
            this.btnGetAdjacentList.Text = "Узнать!";
            this.btnGetAdjacentList.UseVisualStyleBackColor = true;
            this.btnGetAdjacentList.Click += new System.EventHandler(this.btnGetAdjacentList_Click);
            // 
            // tbAdjacentList
            // 
            this.tbAdjacentList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbAdjacentList.Location = new System.Drawing.Point(102, 39);
            this.tbAdjacentList.Name = "tbAdjacentList";
            this.tbAdjacentList.Size = new System.Drawing.Size(100, 26);
            this.tbAdjacentList.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 20);
            this.label12.TabIndex = 8;
            this.label12.Text = "Вершина:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnGetIsEdge);
            this.groupBox5.Controls.Add(this.tbIsEdgeV1);
            this.groupBox5.Controls.Add(this.tbIsEdgeV2);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(6, 36);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(358, 125);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Есть ли ребро?";
            // 
            // btnGetIsEdge
            // 
            this.btnGetIsEdge.Location = new System.Drawing.Point(241, 56);
            this.btnGetIsEdge.Name = "btnGetIsEdge";
            this.btnGetIsEdge.Size = new System.Drawing.Size(100, 29);
            this.btnGetIsEdge.TabIndex = 8;
            this.btnGetIsEdge.Text = "Узнать!";
            this.btnGetIsEdge.UseVisualStyleBackColor = true;
            this.btnGetIsEdge.Click += new System.EventHandler(this.btnGetIsEdge_Click);
            // 
            // tbIsEdgeV1
            // 
            this.tbIsEdgeV1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbIsEdgeV1.Location = new System.Drawing.Point(108, 42);
            this.tbIsEdgeV1.Name = "tbIsEdgeV1";
            this.tbIsEdgeV1.Size = new System.Drawing.Size(100, 26);
            this.tbIsEdgeV1.TabIndex = 1;
            // 
            // tbIsEdgeV2
            // 
            this.tbIsEdgeV2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbIsEdgeV2.Location = new System.Drawing.Point(108, 77);
            this.tbIsEdgeV2.Name = "tbIsEdgeV2";
            this.tbIsEdgeV2.Size = new System.Drawing.Size(100, 26);
            this.tbIsEdgeV2.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(6, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 20);
            this.label15.TabIndex = 23;
            this.label15.Text = "Конец:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(6, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 20);
            this.label14.TabIndex = 22;
            this.label14.Text = "Начало:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnGetBypass);
            this.groupBox7.Controls.Add(this.rtbBypass);
            this.groupBox7.Controls.Add(this.tbBypassVertex);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox7.Location = new System.Drawing.Point(415, 528);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(399, 136);
            this.groupBox7.TabIndex = 26;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Обход";
            // 
            // btnGetBypass
            // 
            this.btnGetBypass.Location = new System.Drawing.Point(233, 17);
            this.btnGetBypass.Name = "btnGetBypass";
            this.btnGetBypass.Size = new System.Drawing.Size(152, 30);
            this.btnGetBypass.TabIndex = 3;
            this.btnGetBypass.Text = "Сделать обход";
            this.btnGetBypass.UseVisualStyleBackColor = true;
            this.btnGetBypass.Click += new System.EventHandler(this.btnGetBypass_Click);
            // 
            // rtbBypass
            // 
            this.rtbBypass.Location = new System.Drawing.Point(10, 61);
            this.rtbBypass.Name = "rtbBypass";
            this.rtbBypass.Size = new System.Drawing.Size(375, 57);
            this.rtbBypass.TabIndex = 2;
            this.rtbBypass.Text = "";
            // 
            // tbBypassVertex
            // 
            this.tbBypassVertex.Location = new System.Drawing.Point(116, 19);
            this.tbBypassVertex.Name = "tbBypassVertex";
            this.tbBypassVertex.Size = new System.Drawing.Size(100, 26);
            this.tbBypassVertex.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Вершина:";
            // 
            // btnParaSoch
            // 
            this.btnParaSoch.Location = new System.Drawing.Point(165, 305);
            this.btnParaSoch.Name = "btnParaSoch";
            this.btnParaSoch.Size = new System.Drawing.Size(139, 34);
            this.btnParaSoch.TabIndex = 27;
            this.btnParaSoch.Text = "Парасочет.";
            this.btnParaSoch.UseVisualStyleBackColor = true;
            this.btnParaSoch.Click += new System.EventHandler(this.btnParaSoch_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 725);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbInfo);
            this.Controls.Add(this.cbTypeFile);
            this.Controls.Add(this.pbGraph);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label1);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.gbInfo.ResumeLayout(false);
            this.gbInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteEdge)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddEdge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteVertex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddVertex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbGraph)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.PictureBox pbGraph;
        private System.Windows.Forms.ComboBox cbTypeFile;
        private System.Windows.Forms.GroupBox gbInfo;
        private System.Windows.Forms.TextBox tbVertexNumber;
        private System.Windows.Forms.Label lblVertexNumber;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox tbEdgeCount;
        private System.Windows.Forms.Label lblEdgeCount;
        private System.Windows.Forms.TextBox tbMaxDegree;
        private System.Windows.Forms.Label lblMaxDegree;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbAddVertex;
        private System.Windows.Forms.TextBox tbAddVertex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblAddVertex;
        private System.Windows.Forms.TextBox tbDeleteVertex;
        private System.Windows.Forms.PictureBox pbDeleteVertex;
        private System.Windows.Forms.PictureBox pbDeleteEdge;
        private System.Windows.Forms.PictureBox pbAddEdge;
        private System.Windows.Forms.TextBox tbAddWeight;
        private System.Windows.Forms.TextBox tbAddV2;
        private System.Windows.Forms.TextBox tbAddV1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbDeleteV2;
        private System.Windows.Forms.TextBox tbDeleteV1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnGetIsEdge;
        private System.Windows.Forms.TextBox tbIsEdgeV1;
        private System.Windows.Forms.TextBox tbIsEdgeV2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbAdjacentList;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnGetAdjacentList;
        private System.Windows.Forms.TextBox tbSvyaz;
        private System.Windows.Forms.TextBox tbCycleNumber;
        private System.Windows.Forms.TextBox tbRadius;
        private System.Windows.Forms.TextBox tbDiameter;
        private System.Windows.Forms.Button IsATree;
        private System.Windows.Forms.Button btnAbsCenter;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RichTextBox rtbBypass;
        private System.Windows.Forms.TextBox tbBypassVertex;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnGetBypass;
        private System.Windows.Forms.Button btnParaSoch;
    }
}

